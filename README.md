# "Graphy" child theme for WordPress #

WordPress child theme for "Graphy" by [Themegraphy](http://themegraphy.com/).

![screenshot.png](http://bytebucket.org/MarkyMarkNo1/wp-theme-graphy-child/raw/7dbc9d131273f96d556c3cd943c202587a7937e1/graphy-custom/screenshot.png)

## Parent theme ##

* ["Graphy" homepage](http://themegraphy.com/graphy-wordpress-theme)
* ["Graphy" demo](http://demo.themegraphy.com/)
* ["Graphy" on WordPress.org](http://wordpress.org/themes/graphy)

## Child theme enhancements ##

* Enables an "Edit" link in entry meta
* Improved Jetpack plugins' [Infinite Scroll](http://jetpack.me/support/infinite-scroll/) support
* [Smart Archives Reloaded](http://wordpress.org/plugins/smart-archives-reloaded/) plugins' list format support

## About WordPress ##

* [WordPress child themes](http://codex.wordpress.org/Child_Themes)
* [WordPress.org](http://wordpress.org/)
