<?php
/**
 * Graphy custom functions
 *
 * @package graphy
 *
 */


/*
 * Adding an "Edit" link to .entry-meta
 *
 * Overriding pluggable function located in inc/template-tags.php
 */

/*
 * Display post header meta.
 *
 * @return void
 */
function graphy_header_meta() {
  // Hide for pages on Search.
  if ( 'post' != get_post_type() ) {
    return;
  }
  ?>
  <div class="entry-meta">
    <span class="posted-on"><?php _e( 'Posted on', 'graphy' ); ?>
      <?php printf( '<a href="%1$s" rel="bookmark"><time class="entry-date published" datetime="%2$s">%3$s</time></a>',
        esc_url( get_permalink() ),
        esc_attr( get_the_date( 'c' ) ),
        esc_html( get_the_date() )
      ); ?>
    </span>
    <?php if ( ! get_theme_mod( 'graphy_hide_author' ) ) : ?>
    <span class="byline"><?php _e( 'by', 'graphy' ); ?>
      <span class="author vcard">
        <?php printf( '<a class="url fn n" href="%1$s">%2$s</a>',
          esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
          esc_html( get_the_author() )
        ); ?>
      </span>
    </span>
    <?php endif; ?>
    <?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
    <span class="comments-link">&middot; <?php comments_popup_link( __( 'Leave a comment', 'graphy' ), __( '1 Comment', 'graphy' ), __( '% Comments', 'graphy' ) ); ?></span>
    <?php endif; ?>
    <?php edit_post_link( __( 'Edit', 'graphy' ), '<span class="edit-link">&middot; ', '</span>' ); ?>
  </div><!-- .entry-meta -->
  <?php
}


/*
 * =====================================================================
 *
 * Plugin Jetpack: Infinite Scroll
 *
 * http://jetpack.me/support/infinite-scroll/
 *
 * =====================================================================
 */

/*
 * Do we have footer widgets?
 *
 * @return bool
 */
function graphy_custom_further_has_footer_widgets( $has_widgets ) {
  if (
    ( ( Jetpack_User_Agent_Info::is_ipad() || ( function_exists( 'jetpack_is_mobile' ) && jetpack_is_mobile() ) ) && is_active_sidebar( 'sidebar' ) ) ||
    is_active_sidebar( 'footer-1' ) ||
    is_active_sidebar( 'footer-2' ) ||
    is_active_sidebar( 'footer-3' ) ||
    is_active_sidebar( 'footer-4' )
  )
    return true;

  return $has_widgets;
}
add_filter( 'infinite_scroll_has_footer_widgets', 'graphy_custom_further_has_footer_widgets' );

/*
 * Custom jetpack_setup action
 *
 * Override support from the parent theme:
 * http://codex.wordpress.org/Function_Reference/remove_theme_support
 */
function graphy_custom_jetpack_setup() {

  /* Remove Graphys' theme support */
  remove_theme_support( 'infinite-scroll' );

  /* Add custom theme support */
  add_theme_support( 'infinite-scroll', array(
    'container' => 'main',
    // 'type'      => 'click',
    'footer'    => 'page',
  ) );

}
/*
 * Use the after_setup_theme hook with a priority of 11 to load after the
 * parent theme, which will fire on the default priority of 10
 */
add_action( 'after_setup_theme', 'graphy_custom_jetpack_setup', 11 );
